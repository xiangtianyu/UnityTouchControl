﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveZoom : MonoBehaviour {

	private float moveSpeed = 0.05f;
	private float zoomThreshold = 4.0f;
	public float perspectiveZoomSpeed = 0.5f;        // The rate of change of the field of view in perspective mode.
	public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.
	public Camera camera;


	void Update()

	{

		//No touch 

		if (Input.touchCount <= 0)
		{
			return;
		}


		//One touch point
		if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			transform.Translate(-touchDeltaPosition.x * moveSpeed, -touchDeltaPosition.y * moveSpeed, 0);
		}


		//multi touch point
		if (Input.touchCount == 2) 
		{
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);
			if (touchZero.phase == TouchPhase.Moved || touchOne.phase == TouchPhase.Moved) {
				// Find the position in the previous frame of each touch.
				Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
				Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

				// Find the magnitude of the vector (the distance) between the touches in each frame.
				float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
				float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

				// Find the difference in the distances between each frame.
				float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

				if (Mathf.Abs (deltaMagnitudeDiff) > zoomThreshold) {

					// If the camera is orthographic...
					if (camera.orthographic) {
						// ... change the orthographic size based on the change in distance between the touches.
						camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

						// Make sure the orthographic size never drops below zero.
						camera.orthographicSize = Mathf.Max (camera.orthographicSize, 0.1f);
					} else {
						// Otherwise change the field of view based on the change in distance between the touches.
						camera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
						// Clamp the field of view to make sure it's between 0 and 180.
						camera.fieldOfView = Mathf.Clamp (camera.fieldOfView, 0.1f, 179.9f);
					}
				} 
			}

		}

	}
}
