﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameManager : MonoBehaviour {
    public static string modelName; // the name of the modelName
                                    // Use this for initialization
	void Start () {
	
	}
	
	// when the model is clicked, the Main scene will start.
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {       
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                modelName = hit.collider.gameObject.name;
                Debug.Log(modelName);
                SceneManager.LoadScene("Move");
            }
        }
	}
}
