﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotate : MonoBehaviour {

	private float moveSpeed = 0.05f;
	private float zoomThreshold = 4.0f;
	private float rotaeSpeed = 0.5f;
	private float scaleSpeed = 0.01f;

	void Update () {
		if (Input.touchCount>=1)
		{   
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit))
			{
				Debug.Log(hit.collider.gameObject.name);
				GameObject obj = hit.collider.gameObject;
			
			if (Input.touchCount == 2) 
			{
				Touch touchZero = Input.GetTouch(0);
				Touch touchOne = Input.GetTouch(1);
				if (touchZero.phase == TouchPhase.Moved || touchOne.phase == TouchPhase.Moved) {
					// Find the position in the previous frame of each touch.
					Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
					Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

					// Find the magnitude of the vector (the distance) between the touches in each frame.
					float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
					float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

					// Find the difference in the distances between each frame.
					float deltaMagnitudeDiff = touchDeltaMag-prevTouchDeltaMag;

					if (Mathf.Abs (deltaMagnitudeDiff) < zoomThreshold) {
						Vector2 prevDir = touchZeroPrevPos - touchOnePrevPos;
						Vector2 currDir = touchZero.position - touchOne.position;
						float yAngle = Vector2.Angle (prevDir, currDir);
						Vector3 cross = Vector3.Cross (prevDir, currDir);
						if (cross.z > 0) {
							yAngle = 360 - yAngle;
						}



						float zAngle = Vector2.Min (touchOnePrevPos - touchOne.position, touchZeroPrevPos - touchZero.position).x * rotaeSpeed;

						float xAngle = -Vector2.Min (touchOnePrevPos - touchOne.position, touchZeroPrevPos - touchZero.position).y * rotaeSpeed;
						obj.transform.Rotate (xAngle, yAngle, zAngle, Space.World);
					} else {
					
						obj.transform.localScale += new Vector3(deltaMagnitudeDiff*scaleSpeed, deltaMagnitudeDiff*scaleSpeed, deltaMagnitudeDiff*scaleSpeed);
					}
				}
			}


			if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
			{
				Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
				obj.transform.Translate(touchDeltaPosition.x * moveSpeed, touchDeltaPosition.y * moveSpeed, 0,Camera.main.transform);
			}
		}
	}
}
}